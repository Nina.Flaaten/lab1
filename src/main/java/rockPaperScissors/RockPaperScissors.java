package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");


    public void run() {

       


        while (true) {
            String computerInput = rpsChoices.get(new Random().nextInt(rpsChoices.size()));
            System.out.println("Let's play round " + roundCounter);
            String userInput;

            //En while-loop som sikrer at spilleren velger riktig alternativ (rock/paper/scissors)
            while (true) {
                userInput = readInput("Your choice (Rock/Paper/Scissors)?");
                userInput = userInput.toLowerCase();
                if (userInput.equals("rock") || userInput.equals("paper") || userInput.equals("scissors")) {
                    break;
                }
                System.out.println("I do not understand " + userInput + ". Could you try again?");
            }

            //Spill logikk, hvem vinner når, poeng
            if (userInput.equals(computerInput)) {
                System.out.println("Human chose " + userInput.toLowerCase() + ", computer chose " + computerInput + ". It's a tie!");

            } else if (userInput.equals("rock")) {
                if (computerInput.equals("paper")) {
                    System.out.println("Human chose " + userInput.toLowerCase() + ", computer chose " + computerInput + ". Computer wins!");
                    computerScore++;
                } else {
                    System.out.println("Human chose " + userInput.toLowerCase() + ", computer chose " + computerInput + ". Human wins!");
                    humanScore++;
                }

            } else if (userInput.equals("paper")) {
                if (computerInput.equals("scissors")) {
                    System.out.println("Human chose " + userInput.toLowerCase() + ", computer chose " + computerInput + ". Computer wins!");
                    computerScore++;
                } else {
                    System.out.println("Human chose " + userInput.toLowerCase() + ", computer chose " + computerInput + ". Human wins!");
                    humanScore++;
                }

            } else if (userInput.equals("scissors")) {
                if (computerInput.equals("rock")) {
                    System.out.println("Human chose " + userInput.toLowerCase() + ", computer chose " + computerInput + ". Computer wins!");
                    computerScore++;
                } else {
                    System.out.println("Human chose " + userInput.toLowerCase() + ", computer chose " + computerInput + ". Human wins!");
                    humanScore++;
                }
            }

            //Printer ut scoren
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);


            //Spille igjen? (y/n)
            String retry = readInput("Do you wish to continue playing? (y/n)? ");
            if (!retry.equals("y")) {
                
                break;
            }
            else {
                roundCounter++;
            }    
            }

            System.out.println("Bye bye :)");



        }
    

    public String readInput (String prompt){
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}