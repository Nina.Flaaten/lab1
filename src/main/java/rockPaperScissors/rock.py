# 20V rock, paper, scissors oppg 14
# dersom inspirasjon, nevne fasiten 



# Let's play round 1
# Your choice (Rock/Paper/Scissors)? Rock My choice was Scissors. You win.
# Score: me 0, you 1
# Continue (y/n)? y
# Let's play round 2
# Your choice (Rock/Paper/Scissors)? Paper My choice was Rock. You win.
# Score: me 0, you 2
# Continue (y/n)? y
# Let's play round 3
# Your choice (Rock/Paper/Scissors)? Papp I don't understand Papp. Try again
# Your choice (Rock/Paper/Scissors)? Paper My choice was Scissors. I win.
# Score: me 1, you 2
# Continue (y/n)? n




# import random

# score_player = 0
# score_computer = 0
# rounds = 0

# while True:
#     rounds += 1
#     print(f"Let's play round {rounds}")
#     user_action = input("Your choice (Rock/Paper/Scissors)? ")
#     possible_actions = ["rock", "paper", "scissors"]
#     computer_action = random.choice(possible_actions)
#     print(f"My choice was {computer_action}.")

#     if user_action == computer_action:
#         print(f"Both players selected {user_action}. It's a tie!")
#     if user_action == "rock":
#         if computer_action == "scissors":
#             print("Rock smashes scissors. You win.")
#             score_player += 1
#             print(f"Score: me {score_computer}, you {score_player}")
#         else:
#             print("Paper covers rock! You lose.")
#             score_computer += 1
#             print(f"Score: me {score_computer}, you {score_player}")
#     if user_action == "paper":
#         if computer_action == "rock":
#             print("My choice was Rock. You win.")
#             score_player += 1
#             print(f"Score: me {score_computer}, you {score_player}")
#         else:
#             print("My choice was Scissors. I win.")
#             score_computer += 1
#             print(f"Score: me {score_computer}, you {score_player}")
#     if user_action == "scissors":
#         if computer_action == "paper":
#             print("Scissors cuts paper! You win!")
#             score_player += 1
#             print(f"Score: me {score_computer}, you {score_player}")
#         else:
#             print("Rock smashes scissors! You lose.")
#             score_computer += 1
#             print(f"Score: me {score_computer}, you {score_player}")
#     # else:
#     #     print(f"I don't understand {user_action}. Try again.")

#     play_again = input("Continue (y/n)? ")
#     if play_again.lower() != "y":
#         break


# fasit:
import random

choices = ["rock", "paper", "scissors"]

def rock_paper_scissors():
    roundCounter = 1
    humanScore = 0
    computerScore = 0
    computerInput = choices[random.randint(0, 2)]

    while True:
        print(f"Let's play round {roundCounter}")

        while True:
            userInput = input("Your choice (Rock/Paper/Scissors)? ").lower()
            try:
                if userInput not in choices:
                    raise Exception
                else:
                    break
            except Exception:
                print(f"I do not understand cardboard. {userInput}. Could you try again?")

        
        if userInput == computerInput:
            print(f"Human chose {userInput}, computer chose {computerInput}. It's a tie!")
        elif userInput == "rock":
            if computerInput == "paper":
                print(f"Human chose {userInput}, computer chose {computerInput}. Computer wins!")
                computerScore += 1
            else:
                print(f"Human chose {userInput}, computer chose {computerInput}. Human wins!")
                humanScore += 1
        elif userInput == "paper":
            if computerInput == "scissors":
                print(f"Human chose {userInput}, computer chose {computerInput}. Computer wins!")
                computerScore += 1
            else:
                print(f"Human chose {userInput}, computer chose {computerInput}. Human wins!")
                humanScore += 1
        elif userInput == "scissors":
            if computerInput == "rock":
                print(f"Human chose {userInput}, computer chose {computerInput}. Computer wins!.")
                computerScore += 1
            else:
                print(f"Human chose {userInput}, computer chose {computerInput}. Human wins!")
                humanScore += 1

        print(f"Score: human {computerScore}, computer {humanScore}")
        cont = input("Do you wish to continue playing? (y/n)? ")
        if cont != 'y': break
        roundCounter += 1
    print("Bye bye :)")

rock_paper_scissors()

